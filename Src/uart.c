#include "uart.h"

#include <stm32f3xx_hal.h>
#include <string.h>

UART_State g_UART_State;
extern UART_HandleTypeDef huart2;

void UART_Init() {
  g_UART_State.tx_mark = 0;
  memset(g_UART_State.send_buf, 0, sizeof(g_UART_State.send_buf));

  g_UART_State.rx_mark = 0;
  g_UART_State.out_buf_cap = 0;
  g_UART_State.out_size = NULL;
  g_UART_State.out_mark = NULL;
}

void UART_SendData(uint8_t* data, size_t len) {
  if (UART_MAX_TX_SIZE < len) {
    return;
  }

  while (g_UART_State.tx_mark || g_UART_State.rx_mark) {
    __NOP();
  }
  g_UART_State.tx_mark = 1;

  memset(g_UART_State.send_buf, 0, sizeof(g_UART_State.send_buf));
  memcpy(g_UART_State.send_buf, data, len);

  HAL_StatusTypeDef res =
      HAL_UART_Transmit_DMA(&huart2, g_UART_State.send_buf, len);
  while (res == HAL_BUSY) {
    res = HAL_UART_Transmit_DMA(&huart2, g_UART_State.send_buf, len);
  }
  while (res != HAL_OK) {
    __NOP();
  }
}

void UART_RecvData(uint8_t* out, size_t out_cap, size_t* real_size,
                   uint8_t* finish_mark) {
  while (g_UART_State.rx_mark || g_UART_State.tx_mark) {
    __NOP();
  }
  g_UART_State.rx_mark = 1;
  *finish_mark = UART_MARK_INPROGRESS;

  memset(out, 0, out_cap);

  g_UART_State.out_buf_cap = out_cap;
  g_UART_State.out_size = real_size;
  g_UART_State.out_mark = finish_mark;

  HAL_StatusTypeDef res = HAL_UART_Receive_DMA(&huart2, out, out_cap);
  while (res == HAL_BUSY) {
    res = HAL_UART_Receive_DMA(&huart2, out, out_cap);
  }
  if (res != HAL_OK) {
    *finish_mark = UART_MARK_HAL_ERROR;
  }
  __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
}
