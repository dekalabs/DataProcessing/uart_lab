#include "mode.h"

#include <string.h>

#include "stm32f3xx_hal.h"
#include "uart.h"

/// LED Utils ///
void led_clear() { HAL_GPIO_WritePin(GPIOE, 0b11111111 << 8, GPIO_PIN_RESET); }

void led_rotate_right() {
  uint8_t mask = (GPIOE->ODR & (0b11111111 << 8)) >> 8;
  mask = (mask << 1) | (mask >> 7);
  led_clear();
  HAL_GPIO_WritePin(GPIOE, mask << 8, GPIO_PIN_SET);
}
void led_rotate_left() {
  uint8_t mask = (GPIOE->ODR & (0b11111111 << 8)) >> 8;
  mask = (mask >> 1) | (mask << 7);
  led_clear();
  HAL_GPIO_WritePin(GPIOE, mask << 8, GPIO_PIN_SET);
}
/////////////////

//// Mode Turn Section ////

/// Enable 3 diods
void ModeTurn_Init(Mode* self) {
  led_clear();
  HAL_GPIO_WritePin(GPIOE, 0b00000111 << 8, GPIO_PIN_SET);
  self->data.turn_data.current_clock = self->data.turn_data.clocks;
}

/// Rotate led mask
void ModeTurn_Update(Mode* self) {
  if (self->data.turn_data.current_clock > 0) {
    /// Do nothing
  } else {
    if (self->data.turn_data.is_left_turn) {
      led_rotate_left();
    } else {
      led_rotate_right();
    }

    self->data.turn_data.current_clock = self->data.turn_data.clocks;
  }

  self->data.turn_data.current_clock--;
}

uint8_t ModeTurn_Next(Mode* self) { return 0; }

Mode MakeModeTurn(uint8_t is_left_turn, uint32_t clocks) {
  Mode m;
  m.data.turn_data.is_left_turn = is_left_turn;
  m.data.turn_data.clocks = clocks;

  m.init = &ModeTurn_Init;
  m.update = &ModeTurn_Update;
  m.next = &ModeTurn_Next;

  return m;
}

///////////////////////////

//// Mode Blink Section ////

void ModeBlink_Init(Mode* self) {
  led_clear();

  self->data.blink_data.curr_clock = self->data.blink_data.clocks;
}

void ModeBlink_Update(Mode* self) {
  if (self->data.blink_data.curr_clock == 0 &&
      self->data.blink_data.times > 0) {
    /// Reset timer
    self->data.blink_data.curr_clock = self->data.blink_data.clocks;
    /// Toggle LED
    HAL_GPIO_TogglePin(GPIOE, 1 << (8 + self->data.blink_data.diod_id));
    /// Reduce allowed switches
    self->data.blink_data.times--;
  }

  if (self->data.blink_data.curr_clock > 0) {
    self->data.blink_data.curr_clock--;
  }
}

uint8_t ModeBlink_Next(Mode* self) {
  return (self->data.blink_data.curr_clock == 0) &&
         (self->data.blink_data.times == 0);
}

Mode MakeModeBlink(uint8_t diod_id, uint32_t times, uint32_t clocks) {
  Mode m;
  m.data.blink_data.clocks = clocks;
  m.data.blink_data.diod_id = diod_id;
  m.data.blink_data.times =
      times * 2;  /// We need 2x toggles to do required times of blinking

  m.init = &ModeBlink_Init;
  m.update = &ModeBlink_Update;
  m.next = &ModeBlink_Next;

  return m;
}

///////////////////////////