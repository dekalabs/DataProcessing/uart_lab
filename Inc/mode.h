#pragma once

#include <stdint.h>

typedef struct ModeTurnData {
  uint8_t is_left_turn;
  uint32_t clocks;

  uint32_t current_clock;
} ModeTurnData;

typedef struct ModeBlinkData {
  uint8_t diod_id;
  uint32_t times;
  uint32_t clocks;

  uint32_t curr_clock;
} ModeBlinkData;

typedef struct Mode Mode;
struct Mode {
  union {
    ModeTurnData turn_data;
    ModeBlinkData blink_data;
  } data;

  void (*init)(Mode*);     /// Init LEDs for this mode
  void (*update)(Mode*);   /// Make 1 tick update
  uint8_t (*next)(Mode*);  /// Tells if mode finished work
                           /// and can be changed to other
};

Mode MakeModeTurn(uint8_t is_left_turn, uint32_t clocks);
Mode MakeModeBlink(uint8_t diod_id, uint32_t times, uint32_t clocks);
