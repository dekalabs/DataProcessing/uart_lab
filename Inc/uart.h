#pragma once

#include <stdint.h>
#include <stdlib.h>

/// Max size for transmiting via UART
#define UART_MAX_TX_SIZE 64

enum UART_FINISH_MARK {
  UART_MARK_UNKNOWN = 0,  /// Default mark
  UART_MARK_INPROGRESS,   /// Read in progress
  UART_MARK_READY,        /// Read completed
  UART_MARK_OVERFLOW,     /// Read completed but overflowed.
  UART_MARK_HAL_ERROR     /// HAL reported a error
};

typedef struct UART_State {
  uint8_t rx_mark;     /// Mark that UART is being used for read data
  size_t out_buf_cap;  /// Capacity of output buffer
  size_t* out_size;    /// Ref to real size of command
  uint8_t* out_mark;   /// Mark of completion

  uint8_t tx_mark;  /// Mark that UART is being used for write data
  uint8_t send_buf[UART_MAX_TX_SIZE];
} UART_State;

extern UART_State g_UART_State;

/// Prepares UART for work
void UART_Init();
/// Async data send.
/// @param data data to send. It must live only on function call.
/// @param len size in bytes
void UART_SendData(uint8_t* data, size_t len);

/// Async data read
/// @param out output array for data. It must live for whole read process
/// @param out_cap capacity of array
/// @param real_size [out] real command size
/// @param finish_mark [out] it will be set to non-zero value when reading
/// complete
void UART_RecvData(uint8_t* out, size_t out_cap, size_t* real_size,
                   uint8_t* finish_mark);
